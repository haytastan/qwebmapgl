# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.2.4 (2020-02-14)


### Bug Fixes

* **preview:** fix preview support with JS modules ([4bd682a](https://gitlab.com/ccrpc/qwebmapgl/commit/4bd682a612035158f589ccb7e329314c48fa5bb9)), closes [#30](https://gitlab.com/ccrpc/qwebmapgl/issues/30)

## [0.2.3] - 2019-11-04
- Fixed error caused by creating QgsPoint from QgsPointXY (#29)

## [0.2.2] - 2019-06-21
- Fixed issue that prevented field checkboxes from updating (#28)
- Fixed error caused by exporting a map with no vector layers (#27)

## [0.2.1] - 2019-06-03
- Added revision to template context and data URLs (#26)

## [0.2.0] - 2019-05-17
- Fixed error on Windows due to file locking (#21)
- Added apply button to export dialog (#19)
- Disabled/enabled field checkboxes based on values of layer checkboxes (#20)
- Fixed issue that prevented layer field checkboxes from updating (#22)
- Added layer and field actions to export dialog (#23)
- Fixed incorrect inclusion of layer in tiles (#24)
- Fixed issue: map ID is set to a single character when typing the title (#25)

## [0.1.0] - 2019-05-02
- Initial release
