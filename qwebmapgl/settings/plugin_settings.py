import os
from PyQt5.QtCore import QSettings
from qgis.core import QgsApplication
from .base_map import BaseMap
from .base_setting import BaseSetting
from .object_array_setting import ObjectArraySetting
from .string_setting import StringSetting
from .template import Template


default_templates_folder = os.path.join(
    QgsApplication.instance().qgisSettingsDirPath(), 'qwebmapgl', 'templates')


class PluginSettings:

    base_maps = ObjectArraySetting('base_maps', BaseMap, ['name', 'url'])
    base_path = StringSetting('base_path', '')
    base_url = StringSetting('base_url', '/')
    glyphs_url = StringSetting('glyphs_url', '/')
    templates_folder = StringSetting(
        'templates_folder', default_templates_folder)

    def __init__(self):
        self.values = {}
        self.settings = QSettings()
        for setting in self._settings:
            setting.read(self)

    @property
    def _settings(self):
        for value in self.__class__.__dict__.values():
            if isinstance(value, BaseSetting):
                yield value

    @property
    def default_base_map(self):
        for base_map in self.base_maps:
            return base_map

    @property
    def default_template(self):
        for template in self.templates:
            return template

    @property
    def templates(self):
        if not os.path.isdir(self.templates_folder):
            return []

        for id in os.listdir(self.templates_folder):
            template = self.get_template(id)
            if template:
                yield template

    def get_template(self, id):
        template_dir = os.path.join(self.templates_folder, id)
        template_config = os.path.join(template_dir, 'template.json')
        if os.path.isfile(template_config):
            return Template(id, template_dir)

    def save(self):
        for setting in self._settings:
            setting.write(self)
