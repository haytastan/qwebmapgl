from copy import copy


class BaseSetting:
    def __init__(self, name):
        self.name = name
        self.default = None

    def __get__(self, obj, type=None):
        return obj.values.get(self.name, copy(self.default))

    def __set__(self, obj, value):
        obj.values[self.name] = value
