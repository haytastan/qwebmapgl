

class WebMapField:

    def __init__(self, index, name, **kwargs):
        self.index = index
        self.name = name
        self.alias = kwargs.get('alias', True)
        self.map = kwargs.get('map', True)
        self.download = kwargs.get('download', True)
