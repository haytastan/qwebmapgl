from qgis.core import QgsProject
from .field import WebMapField


def to_bool(value):
    if isinstance(value, str):
        return value == 'True'
    return bool(value)


class WebMapLayer:
    _settings = ['id', 'map', 'legend', 'download', 'popup', 'min_zoom',
                 'max_zoom']

    def __init__(self, map, layer, **kwargs):
        self._map = map
        self._layer = layer
        self._read_settings()
        self.fields = list(self._create_fields())
        self.overzoom = kwargs.get('overzoom', 14)

    def _read(self, name, default, converter=None):
        if converter is None:
            converter = type(default)
        return converter(
            self._layer.customProperty('qwebmapgl/' + name, default))

    def _write(self, name, value):
        self._layer.setCustomProperty('qwebmapgl/' + name, str(value))

    def _read_settings(self):
        self.map = self._read('map', True, to_bool)
        self.legend = self._read('legend', True, to_bool)
        self.download = self._read('download', True, to_bool)
        self.popup = self._read('popup', False, to_bool)

        # TODO: Calculate smarter defaults for zoom levels based on
        # scale-dependent rendering settings for the layer.
        self.min_zoom = int(self._read('min_zoom', 0))
        self.max_zoom = int(self._read('max_zoom', 24))

    def _write_settings(self):
        for setting in self._settings:
            self._write(setting, getattr(self, setting))

        for field in self.fields:
            self._layer.setFieldAlias(field.index, field.alias)

        self._layer.setExcludeAttributesWms(
            set([f.name for f in self.fields if not f.map]))

        self._layer.setExcludeAttributesWfs(
            set([f.name for f in self.fields if not f.download]))

    def _create_fields(self):
        wms_exclude = self._layer.excludeAttributesWms()
        wfs_exclude = self._layer.excludeAttributesWfs()

        for i, field in enumerate(self._layer.fields().toList()):
            yield WebMapField(
                i,
                field.name(),
                alias=field.alias(),
                map=field.name() not in wms_exclude,
                download=field.name() not in wfs_exclude)

    @property
    def id(self):
        return self._layer.id()

    @property
    def name(self):
        return self._layer.name()

    @property
    def qgis_layer(self):
        return self._layer

    @property
    def map_zoom(self):
        min_zoom = max(0, self.min_zoom)
        max_zoom = max(min(self.max_zoom, 24), min_zoom)

        return (min_zoom, max_zoom)

    @property
    def tile_zoom(self):
        min_zoom = max(0, self.min_zoom)
        max_zoom = max(min(self.overzoom, self.max_zoom - 1), min_zoom)

        return (min_zoom, max_zoom)

    @property
    def download_attributes(self):
        return [f.index for f in self.fields if f.download]

    @property
    def map_attributes(self):
        return [f.index for f in self.fields if f.map]

    @property
    def visible(self):
        lt_root = QgsProject.instance().layerTreeRoot()
        lt_layer = lt_root.findLayer(self.id)
        return lt_layer.isVisible()

    def save(self):
        self._write_settings()
