import webbrowser
from .base_task import BaseTask


class ExportTask(BaseTask):
    """Task for the overall export operation"""

    def __init__(self, description, options):
        super().__init__(description)
        self._options = options

    def _run(self):
        return True

    def finished(self, result):
        super().finished(result)
        if self._options.open_browser:
            webbrowser.open_new(self._options.base_url)
