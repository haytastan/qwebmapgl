import os
from .base_task import BaseTask


class CreateFoldersTask(BaseTask):
    """Task to create web map output folders"""

    def __init__(self, description, options):
        super().__init__(description)
        self._options = options

    def _run(self):
        opts = self._options
        for folder_name in [opts.data_dir, opts.legend_dir, opts.style_dir]:
            if self.isCanceled():
                return False
            folder = os.path.join(opts.base_path, folder_name)
            if not os.path.exists(folder):
                os.makedirs(folder)
        return True
