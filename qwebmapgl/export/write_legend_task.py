from .base_task import BaseTask


class WriteLegendTask(BaseTask):
    """Task to export legend images"""

    def __init__(self, description, options, style):
        super().__init__(description)
        self._options = options
        self._style = style

    def _run(self):
        self._style.write_legend(
            self._options.legend_path,
            self._options.legend_format)
