from .base_task import BaseTask


class WriteSpriteTask(BaseTask):
    """Task to export a sprite"""

    def __init__(self, description, style, scale, options):
        super().__init__(description)
        self._style = style
        self._scale = scale
        self._options = options

    def _run(self):
        self._style.write_sprite(self._options.style_dir_path, self._scale)
