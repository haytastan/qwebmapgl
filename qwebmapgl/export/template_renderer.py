import os
from jinja2 import Environment, FileSystemLoader, select_autoescape
from ..utils import urljoin


PLUGIN_TEMPLATES = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'templates')


class TemplateRenderer:
    def __init__(self, options, layer_map={}, legend=None, revision=1):
        self._options = options
        self._layer_map = layer_map
        self._legend = legend
        self._revision = revision
        self._template_env = self._get_env(self._map.template.folder)
        self._context = self._get_context()

    @property
    def _map(self):
        return self._options.map

    @property
    def _settings(self):
        return self._options.settings

    def _get_env(self, path):
        return Environment(
            loader=FileSystemLoader([path, PLUGIN_TEMPLATES]),
            autoescape=select_autoescape(['html', 'xml'])
        )

    def _serialize_attributes(self, layer):
        for field in sorted(layer.fields, key=lambda field: field.index):
            yield {
                'index': field.index,
                'name': field.name,
                'alias': field.alias,
                'map': field.map,
                'download': field.download,
            }

    def _serialize_basemaps(self):
        for base_map in self._settings.base_maps:
            yield {
                'name': base_map.name,
                'url': base_map.url,
                'enabled': base_map.url == self._map.base_map_url
            }

    def _serialize_layers(self):
        for layer in self._map.layers:
            yield {
                'id': layer.id,
                'download': layer.download,
                'layers': self._layer_map.get(layer.id, []),
                'legend': layer.legend,
                'popup': layer.popup,
                'name': layer.name,
                'visible': layer.visible,
                'attributes': list(self._serialize_attributes(layer)),
            }

    def _serialize_legend(self):
        if not self._legend:
            return []

        for item in self._legend.legend_items:
            image = None
            if item.has_image():
                image = urljoin(
                    self._options.legend_url,
                    item.get_filename(self._options.legend_format)
                )
            yield {
                'label': item.label,
                'layers': item.layers,
                'image': image
            }

    def _serialize_options(self):
        return {
            'base_url': self._options.base_url.rstrip('/'),
            'data_url': self._options.data_url,
            'latitude': self._map.center_latitude,
            'longitude': self._map.center_longitude,
            'preview': self._options.preview,
            'revision': self._revision,
            'style_url': self._options.style_url,
            'title': self._map.title,
            'zoom': self._map.initial_zoom,
        }

    def _get_context(self):
        return {
            'basemaps': list(self._serialize_basemaps()),
            'layers': list(self._serialize_layers()),
            'legend': list(self._serialize_legend()),
            'options': self._serialize_options(),
        }

    def stream(self, filename):
        template = self._template_env.get_template(filename)
        return template.stream(self._context)
