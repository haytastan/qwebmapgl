import json
from .base_task import BaseTask


class WriteStyleTask(BaseTask):
    """Task to write a style object to a JSON file"""

    def __init__(self, description, style, path):
        super().__init__(description)
        self._style = style
        self._path = path

    def _run(self):
        with open(self._path, 'w') as json_file:
            json.dump(self._style.get_json(), json_file)
