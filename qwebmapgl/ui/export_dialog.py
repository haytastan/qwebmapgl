import os
import re
from PyQt5 import QtCore, QtGui, QtWidgets
from .proxy import ModelProxy
from .empty_layer import EmptyLayer
from .table_column import TableColumn
from .table_model import TableModel
from .utils import load_form_ui, add_menu_action
from ..preview import WebMapPreview
from ..utils import tr


FORM_CLASS, _ = load_form_ui('export_dialog.ui')


class ExportDialog(QtWidgets.QDialog, FORM_CLASS):

    def __init__(self, map, settings, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self._map = map
        self._settings = settings

        # Set up preview
        self._preview = WebMapPreview(self._map, self._settings)
        self._preview.start()

        # Add dialog buttons
        self.applyButton = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.Apply)
        self.exportButton = self.buttonBox.addButton(
            tr('Export'), QtWidgets.QDialogButtonBox.AcceptRole)

        # Tweak UI settings
        self.centerZoomButton.setIcon(
            self.centerZoomButton.style().standardIcon(
                QtWidgets.QStyle.SP_BrowserReload))

        # Populate choices
        self._set_layers()
        self._set_templates()
        self._set_base_maps()

        # Create proxies
        self._map_proxy = ModelProxy(
            map, self,
            template_id='templateComboBox',
            base_map_url='baseMapComboBox',
            title='titleLineEdit',
            center_longitude='lonDoubleSpinBox',
            center_latitude='latDoubleSpinBox',
            initial_zoom='zoomDoubleSpinBox',
            id='idLineEdit')

        self._layer_proxy = ModelProxy(
            self._get_current_layer(), self,
            map='mapCheckBox',
            legend='legendCheckBox',
            download='downloadCheckBox',
            popup='popupCheckBox',
            min_zoom='minZoomSpinBox',
            max_zoom='maxZoomSpinBox')

        # Set up fields table
        self._update_fields()
        self.fieldTableView.setColumnWidth(0, 100)

        # Set default map ID based on title
        self._update_map_id()

        # Connect additional event handlers
        self.templateComboBox.activated.connect(self._update_dialog_buttons)
        self.baseMapComboBox.activated.connect(self._update_dialog_buttons)
        self.idLineEdit.textChanged.connect(self._update_dialog_buttons)
        self.centerZoomButton.clicked.connect(self._use_preview_position)
        self.layerComboBox.currentIndexChanged.connect(
            self._update_layer_proxy)
        self.titleLineEdit.editingFinished.connect(self._update_map_id)
        self.mapCheckBox.stateChanged.connect(self._update_fields)
        self.mapCheckBox.stateChanged.connect(
            self._update_legend_checkbox_enabled)
        self.downloadCheckBox.stateChanged.connect(self._update_fields)
        self.applyButton.clicked.connect(self._apply)
        self.previewButton.clicked.connect(self._show_preview)
        self.finished.connect(self._unload)

        # Set up validators
        id_re = QtCore.QRegularExpression(r'[a-z0-9\-]+')
        id_validator = QtGui.QRegularExpressionValidator(id_re)
        self.idLineEdit.setValidator(id_validator)

        # Check whether initial values are valid
        self._update_dialog_buttons()

        # Set button menus
        self._set_button_menus()

    def _get_current_layer(self):
        idx = self.layerComboBox.currentIndex()
        if idx >= 0:
            return self.layerComboBox.itemData(idx)
        return EmptyLayer()

    def _set_layers(self):
        self.layerComboBox.clear()

        for layer in self._map.layers:
            self.layerComboBox.addItem(layer.name, layer)

        if self.layerComboBox.count() > 0:
            self.layerComboBox.setCurrentIndex(0)

    def _set_base_maps(self):
        for i, base_map in enumerate(self._settings.base_maps):
            self.baseMapComboBox.addItem(base_map.name, base_map.url)

    def _set_templates(self):
        for i, template in enumerate(self._settings.templates):
            self.templateComboBox.addItem(template.name, template.id)

    def _set_button_menus(self):
        layers_menu = QtWidgets.QMenu()
        add_menu_action(
            layers_menu,
            'Enable all layers',
            self._enable_layers)
        add_menu_action(
            layers_menu,
            'Disable all layers',
            self._disable_layers)
        add_menu_action(
            layers_menu,
            'Enable and disable layers based on visibility',
            self._visibility_layers)
        self.layersButton.setMenu(layers_menu)

        fields_menu = QtWidgets.QMenu()
        add_menu_action(
            fields_menu,
            'Check map checkbox for all fields',
            self._fields_map_all)
        add_menu_action(
            fields_menu,
            'Uncheck map checkbox for all fields',
            self._fields_map_none)
        fields_menu.addSeparator()
        add_menu_action(
            fields_menu,
            'Check download checkbox for all fields',
            self._fields_download_all)
        add_menu_action(
            fields_menu,
            'Uncheck download checkbox for all fields',
            self._fields_download_none)
        self.fieldsButton.setMenu(fields_menu)

    def _set_layer_enabled(self, layer, enabled):
        layer.map = enabled
        layer.legend = enabled
        layer.download = enabled

    def _set_layers_enabled(self, enabled):
        for layer in self._map.layers:
            self._set_layer_enabled(layer, enabled)
        self._update_layer_proxy()

    def _enable_layers(self):
        self._set_layers_enabled(True)

    def _disable_layers(self):
        self._set_layers_enabled(False)

    def _visibility_layers(self):
        for layer in self._map.layers:
            self._set_layer_enabled(layer, layer.visible)
        self._update_layer_proxy()

    def _set_fields_checkboxes(self, column, value):
        model = self.fieldTableView.model()
        for row in range(model.rowCount()):
            index = model.createIndex(row, column)
            value = QtCore.Qt.Checked if value else QtCore.Qt.Unchecked
            model.setData(index, value, QtCore.Qt.CheckStateRole)

    def _fields_map_all(self):
        self._set_fields_checkboxes(1, True)

    def _fields_map_none(self):
        self._set_fields_checkboxes(1, False)

    def _fields_download_all(self):
        self._set_fields_checkboxes(2, True)

    def _fields_download_none(self):
        self._set_fields_checkboxes(2, False)

    def _update_layer_proxy(self):
        self._layer_proxy.set_model(self._get_current_layer())
        self._update_fields()
        self._update_legend_checkbox_enabled()

    def _update_fields(self):
        layer = self._get_current_layer()
        field_model = TableModel(layer.fields, [
            TableColumn('name', 'Field Name', editable=False),
            TableColumn(
                'map', 'Map',
                checkbox=True, enabled=self._layer_proxy.map),
            TableColumn(
                'download', 'Download',
                checkbox=True, enabled=self._layer_proxy.download),
            TableColumn('alias', 'Alias'),
        ])
        self.fieldTableView.setModel(field_model)

    def _use_preview_position(self):
        longitude, latitude, zoom = self._preview.get_position()
        self._map_proxy.center_longitude = longitude
        self._map_proxy.center_latitude = latitude
        self._map_proxy.initial_zoom = zoom

    def _update_map_id(self):
        if self._map_proxy.id:
            return

        self._map_proxy.id = re.sub(
            r'[^0-9a-z]+', '-', self._map_proxy.title.lower())

    def _unload(self):
        self._preview.stop()

    def _show_preview(self):
        self._preview.update()

    def _confirm_overwrite(self, out_path):
        box = QtWidgets.QMessageBox(self)
        box.setText(tr('The map {} already exists.').format(out_path))
        box.setInformativeText(tr('Do you want to overwrite it?'))
        box.setStandardButtons(
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel)
        box.setDefaultButton(QtWidgets.QMessageBox.Yes)
        return box.exec() == QtWidgets.QMessageBox.Yes

    def _update_dialog_buttons(self):
        save = True
        preview = True

        if not self._map_proxy.id:
            save = False

        if not self._map.template or not self._map_proxy.base_map_url:
            save = False
            preview = False

        self.exportButton.setEnabled(save)
        self.applyButton.setEnabled(save)
        self.previewButton.setEnabled(preview)

    def _update_legend_checkbox_enabled(self):
        self.legendCheckBox.setEnabled(self._layer_proxy.map)

    def _apply(self):
        self._map.save()

    def done(self, value):
        if (value == QtWidgets.QDialog.Accepted):
            # Save settings
            self._apply()
            # Check if output folder already exists
            out_path = os.path.join(
                self._settings.base_path, self._map_proxy.id)
            if os.path.exists(out_path):
                if self._confirm_overwrite(out_path):
                    if os.path.isfile(out_path):
                        os.remove(out_path)
                    return super().done(value)
                else:
                    return

        return super().done(value)
