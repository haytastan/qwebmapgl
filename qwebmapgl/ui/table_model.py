from PyQt5.QtCore import QAbstractTableModel, QVariant, QModelIndex, Qt


class TableModel(QAbstractTableModel):
    def __init__(self, items, cols, cls=None, parent=None):
        QAbstractTableModel.__init__(self, parent=parent)
        self._items = items
        self._cols = cols
        self._cls = cls

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self._cols[section].label
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()

        col = self._cols[index.column()]
        allowed_roles = []
        if not col.checkbox:
            allowed_roles.append(Qt.DisplayRole)
        if col.editable and not col.checkbox:
            allowed_roles.append(Qt.EditRole)
        if col.checkbox:
            allowed_roles.append(Qt.CheckStateRole)

        if role not in allowed_roles:
            return QVariant()

        item = self._items[index.row()]
        value = getattr(item, col.attr)

        if col.checkbox:
            value = Qt.Checked if value else Qt.Unchecked

        return QVariant(value)

    def flags(self, index):
        if not index.isValid():
            return 0
        col = self._cols[index.column()]

        flags = Qt.ItemFlags()
        if col.enabled:
            flags = Qt.ItemIsEnabled
        if col.editable and not col.checkbox:
            flags |= Qt.ItemIsEditable | Qt.ItemIsSelectable
        if col.checkbox:
            flags |= Qt.ItemIsUserCheckable

        return flags

    def setData(self, index, value, role):
        if not index.isValid():
            return False

        col = self._cols[index.column()]
        item = self._items[index.row()]

        if col.checkbox and role == Qt.CheckStateRole:
            setattr(item, col.attr, value == Qt.Checked)
            self.dataChanged.emit(index, index, [role])
            return True
        elif col.editable and role == Qt.EditRole:
            setattr(item, col.attr, value)
            self.dataChanged.emit(index, index, [role])
            return True
        return False

    def rowCount(self, parent=QModelIndex()):
        return len(self._items)

    def columnCount(self, parent=QModelIndex()):
        return len(self._cols)

    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row + count - 1)
        for offset in range(count):
            self._items.insert(row + offset, self._cls())
        self.endInsertRows()
        return True

    def removeRows(self, row, count, parent=QModelIndex()):
        if row + count - 1 > len(self._items):
            return False

        self.beginRemoveRows(parent, row, row + count - 1)
        del self._items[row:(row + count)]
        self.endRemoveRows()
        return True
