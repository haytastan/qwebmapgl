from PyQt5 import QtWidgets
from .utils import load_form_ui
from ..settings import BaseMap
from .proxy import ModelProxy
from .table_column import TableColumn
from .table_model import TableModel


FORM_CLASS, _ = load_form_ui('settings_dialog.ui')


class SettingsDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, settings, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self._base_maps_model = TableModel(settings.base_maps, [
            TableColumn('name', 'Base Map Name'),
            TableColumn('url', 'Style URL'),
        ], BaseMap)
        self.baseMapsTableView.setModel(self._base_maps_model)

        self._settings_proxy = ModelProxy(
            settings, self,
            base_path='basePathLineEdit',
            base_url='baseUrlLineEdit',
            glyphs_url='glyphsUrlLineEdit',
            templates_folder='templatesLineEdit')

        self.addBaseMapButton.clicked.connect(
            lambda: self._insert_row(self._base_maps_model))
        self.deleteBaseMapButton.clicked.connect(
            lambda: self._remove_rows(self.baseMapsTableView))
        self.basePathBrowseButton.clicked.connect(
            lambda: self._choose_directory('base_path'))
        self.templatesBrowseButton.clicked.connect(
            lambda: self._choose_directory('templates_folder'))

        self.baseMapsTableView.setColumnWidth(0, 200)

    def _choose_directory(self, prop):
        new_dir = QtWidgets.QFileDialog.getExistingDirectory(
            directory=getattr(self._settings_proxy, prop))

        if new_dir:
            setattr(self._settings_proxy, prop, new_dir)

    def _insert_row(self, model):
        model.insertRow(model.rowCount())

    def _remove_rows(self, view):
        rows = set([i.row() for i in view.selectedIndexes()])
        for row in sorted(rows, reverse=True):
            view.model().removeRow(row, view.rootIndex())
