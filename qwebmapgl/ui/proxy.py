from PyQt5 import QtWidgets as QW


class ModelProxy:
    def __init__(self, model, view, **kwargs):
        self._view = view
        self._mapping = kwargs
        self.set_model(model)
        self._bind()

    def __getattr__(self, name):
        if name.startswith('_'):
            return super().__getattr__(name)
        return getattr(self._model, name)

    def __setattr__(self, name, value):
        if name.startswith('_'):
            super().__setattr__(name, value)
        else:
            setattr(self._model, name, value)
            self._update_view(name, value)

    def set_model(self, model):
        self._model = model
        for name in self._mapping.keys():
            self._update_view(name, getattr(self._model, name))

    def _get_widget(self, model_name):
        view_name = self._mapping[model_name]
        return getattr(self._view, view_name)

    def _get_handler(self, model_name):
        def handler():
            self._update_model(model_name)

        return handler

    def _bind(self):
        for (model_name, view_name) in self._mapping.items():
            widget = getattr(self._view, view_name)
            handler = self._get_handler(model_name)

            if isinstance(widget, QW.QComboBox):
                widget.activated.connect(handler)
            elif isinstance(widget, QW.QCheckBox):
                widget.stateChanged.connect(handler)
            elif isinstance(widget, (QW.QSpinBox, QW.QDoubleSpinBox)):
                widget.valueChanged.connect(handler)
            else:
                widget.textChanged.connect(handler)

    def _update_model(self, model_name):
        widget = self._get_widget(model_name)
        if isinstance(widget, QW.QComboBox):
            value = widget.currentData()
        elif isinstance(widget, QW.QCheckBox):
            value = widget.isChecked()
        elif isinstance(widget, (QW.QSpinBox, QW.QDoubleSpinBox)):
            value = widget.value()
        else:
            value = widget.text()

        setattr(self._model, model_name, value)

    def _update_view(self, model_name, value):
        widget = self._get_widget(model_name)
        if isinstance(widget, QW.QComboBox):
            for i in range(widget.count()):
                if widget.itemData(i) == value:
                    widget.setCurrentIndex(i)
                    break
        elif isinstance(widget, QW.QCheckBox):
            widget.setChecked(value)
        elif isinstance(widget, (QW.QSpinBox, QW.QDoubleSpinBox)):
            widget.setValue(value)
        else:
            widget.setText(value)
