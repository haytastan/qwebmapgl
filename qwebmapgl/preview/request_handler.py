import json
import re
import sqlite3
from http.server import SimpleHTTPRequestHandler
from urllib.parse import urlparse

TILE_RE = re.compile(r'^.+/(\d+)/(\d+)/(\d+)\.pbf$')


class PreviewRequestHandler(SimpleHTTPRequestHandler):

    def __init__(self, request, client_address, server):
        self._connection = sqlite3.connect(server.tile_path)
        self._parsed_path = None
        super().__init__(request, client_address, server)

    @property
    def _parsed(self):
        if self._parsed_path is None:
            self._parsed_path = urlparse(self.path)
        return self._parsed_path

    def _zxy(self):
        match = TILE_RE.match(self._parsed.path)
        if match:
            return [int(g) for g in match.groups()]
        return [None, None, None]

    def _get_tile_data(self, x, y, z):
        if x is None:
            return None

        # Convert to TMS row numbering.
        y = 2**z - 1 - y

        cur = self._connection.cursor()
        cur.execute('SELECT tile_data FROM tiles WHERE '
                    'tile_column = ? AND tile_row = ? AND zoom_level = ?',
                    (x, y, z))

        row = cur.fetchone()
        if row:
            return row[0]

    def _head_tile(self):
        z, x, y = self._zxy()
        tile_data = self._get_tile_data(x, y, z)
        if tile_data is not None:
            self.send_response(200)
            self.send_header('Content-Encoding', 'gzip')
            self.send_header('Content-Type', 'application/x-protobuf')
            self.send_header('Last-Modified', self.date_time_string())
        elif x is None:
            self.send_response(404)
        else:
            self.send_response(204)
        self.end_headers()
        return tile_data

    def _head_tilejson(self):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Last-Modified', self.date_time_string())
        self.end_headers()

    def _get_tile(self):
        tile_data = self._head_tile()
        if tile_data:
            self.wfile.write(tile_data)

    def _get_tilejson(self):
        qs = f'?{self._parsed.query}' if self._parsed.query else ''
        tile_url = ''.join([
            self.server.base_url,
            self.server.tile_base_url_relative,
            '/{z}/{x}/{y}.pbf' + qs
        ])
        data = {
            'tilejson': '2.2.0',
            'name': self.server.tile_name,
            'scheme': 'xyz',
            'tiles': [tile_url]
        }

        cur = self._connection.cursor()
        cur.execute('SELECT name, value FROM metadata')
        meta = dict(cur.fetchall())

        if 'attribution' in meta:
            data['attribution'] = meta['attribution']

        if 'description' in meta:
            data['description'] = meta['description']

        if 'minzoom' in meta:
            data['minzoom'] = int(meta['minzoom'])

        if 'maxzoom' in meta:
            data['maxzoom'] = int(meta['maxzoom'])

        if 'center' in meta:
            data['center'] = json.loads('[{}]'.format(meta['center']))

        if 'bounds' in meta:
            data['bounds'] = json.loads('[{}]'.format(meta['bounds']))

        if 'version' in meta:
            data['version'] = meta['version']

        if 'json' in meta:
            json_meta = json.loads(meta['json'])
            if 'vector_layers' in json_meta:
                data['vector_layers'] = json_meta['vector_layers']

        self._head_tilejson()
        self.wfile.write(json.dumps(data).encode())

    def _do(self, name, tilejson, tile):
        if self._parsed.path == self.server.tilejson_url_relative:
            return tilejson()
        elif self._parsed.path.startswith(self.server.tile_base_url_relative):
            return tile()
        return getattr(super(), name)()

    def do_HEAD(self):
        return self._do('do_HEAD', self._head_tilejson, self._head_tile)

    def do_GET(self):
        return self._do('do_GET', self._get_tilejson, self._get_tile)

    def do_POST(self):
        if self._parsed.path == '/qwebmapgl-position':
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            try:
                data = [float(d) for d in json.loads(data_string)]
            except (ValueError, TypeError):
                return self.send_response(400)
            if len(data) == 3:
                self.server.position = data
                return self.send_response(200)
            else:
                return self.send_response(400)

        super().do_POST()

    def log_message(self, format, *args):
        # Do not attempt to wite output to the stderr.
        # Doing so causes the HTTP request to fail on Windows.
        pass
