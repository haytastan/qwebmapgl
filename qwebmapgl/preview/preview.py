import os
import threading
from tempfile import TemporaryDirectory
from .request_handler import PreviewRequestHandler
from .server import PreviewHTTPServer
from ..export import Exporter, ExportOptions
from qgis.core import QgsMessageLog, Qgis


def deferred_cleanup(dir, attempts):
    try:
        dir.cleanup()
    except PermissionError:
        if attempts > 1:
            timer = threading.Timer(1, deferred_cleanup, (dir, attempts - 1))
            timer.start()


class WebMapPreview:
    def __init__(self, map, settings, host='localhost', port=0):
        self._map = map
        self._settings = settings
        self._host = host
        self._port = port
        self._temp_dir = TemporaryDirectory()

        self._server = None
        self._server_thread = None

        self._options = ExportOptions(
            map, settings,
            base_path=self._temp_dir.name,
            open_browser=True,
            preview=True)
        self._exporter = Exporter(self._options)

    def start(self):
        self._cwd = os.getcwd()
        os.chdir(self._temp_dir.name)
        self._server = PreviewHTTPServer(
            (self._host, self._port), PreviewRequestHandler, self._options)
        self._server_thread = threading.Thread(
            target=self._server.serve_forever, daemon=True)
        self._server_thread.start()
        self._options.base_url = self._server.base_url

    def update(self):
        if self._map.template:
            self._exporter.export()

    def get_position(self):
        if self._server:
            return self._server.position
        return [0, 0, 0]

    def stop(self):
        if self._server:
            os.chdir(self._cwd)
            self._server.shutdown()
            self._server.socket.close()
            self._server_thread.join()
            deferred_cleanup(self._temp_dir, 60)
