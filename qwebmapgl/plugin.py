import os.path
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction
from qgis.core import QgsApplication, QgsProject
from .export import Exporter, ExportOptions
from .ui import ExportDialog, SettingsDialog
from .utils import tr
from .settings import PluginSettings
from .webmap import WebMap


class WebMapGLExporter:
    def __init__(self, iface):
        self.iface = iface
        self.plugin_dir = os.path.dirname(__file__)
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'WebMapGLExporter_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        self.menu = tr('&Web Map')

    def initGui(self):
        parent = self.iface.mainWindow()

        export_icon = QIcon(
            os.path.join(self.plugin_dir, 'resources', 'icon.svg'))

        self.export_action = QAction(
            export_icon, tr('Export Web Map…'), parent)
        self.export_action.triggered.connect(self.show_export_dialog)
        self.iface.addPluginToWebMenu(self.menu, self.export_action)
        self.iface.addWebToolBarIcon(self.export_action)

        settings_icon = QgsApplication.getThemeIcon('/mActionOptions.svg')

        self.settings_action = QAction(
            settings_icon, tr('Web Map Settings…'), parent)
        self.settings_action.triggered.connect(self.show_settings_dialog)
        self.iface.addPluginToWebMenu(self.menu, self.settings_action)

    def unload(self):
        self.iface.removePluginWebMenu(self.menu, self.export_action)
        self.iface.removePluginWebMenu(self.menu, self.settings_action)
        self.iface.removeWebToolBarIcon(self.export_action)

    def show_export_dialog(self):
        settings = PluginSettings()
        map = WebMap(QgsProject.instance(), self.iface.mapCanvas(), settings)

        dialog = ExportDialog(map, settings)
        dialog.setModal(True)
        dialog.show()

        export = dialog.exec_()
        if export:
            options = ExportOptions(map, settings)
            self._exporter = Exporter(options)
            self._exporter.export()

    def show_settings_dialog(self):
        settings = PluginSettings()
        dialog = SettingsDialog(settings)
        dialog.setModal(True)
        dialog.show()

        save = dialog.exec_()
        if save:
            settings.save()
